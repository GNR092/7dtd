FROM gnr092/steamcmd:latest AS base

LABEL maintainer="genecan092@gmail.com"
ENV PGID 1000
ENV PUID 1000
#USER root

#ARG DEBIAN_FRONTEND=noninteractive
USER steam
# Puertos
EXPOSE 25000/tcp
EXPOSE 25000/udp
EXPOSE 25001/udp
EXPOSE 25002/udp
EXPOSE 25003/udp
EXPOSE 8080/tcp
EXPOSE 8081/tcp
RUN set -x && mkdir -p "${STEAMCMDDIR}/7dtd"
COPY entry.sh ${SERVERDIR}/entry.sh
ENV SERVERDIR ${STEAMCMDDIR}/7dtd

VOLUME [ "${SERVERDIR}" ]

#variables del server
ENV SERVERCONFIG="${SERVERDIR}/serverconfig.xml"

WORKDIR ${STEAMCMDDIR}

SHELL [ "bin/bash", "-c"]

